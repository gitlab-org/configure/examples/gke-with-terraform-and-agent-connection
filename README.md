# Infrastructure as Code with GitLab and Terraform (GKE)

This repository contains sample code for creating Google Kubernetes Engine (GKE) with the [GitLab Infrastructure as Code](https://docs.gitlab.com/ee/user/infrastructure/) and connecting it to GitLab using the GitLab Kubernetes Agent.

For more information on how to use it, please refer to the [official docs](https://docs.gitlab.com/ee/user/infrastructure/clusters/connect/new_gke_cluster.html).
