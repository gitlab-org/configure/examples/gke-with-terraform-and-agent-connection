output "env-dynamic-url" {
  value = "https://${google_container_cluster.primary.endpoint}"
}

output "agent_id" {
  value     = module.gitlab_kubernetes_agent_registration.agent_id
}

output "token_secret" {
  value     = module.gitlab_kubernetes_agent_registration.token_secret
  sensitive = true
}
