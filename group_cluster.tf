provider "gitlab" {
    token = var.gitlab_token
}

module "gitlab_kubernetes_agent_registration" {
  source = "gitlab.com/nagyv-gitlab/kubernetes-agent-terraform-register-agent/local"
  version = "0.0.2"

  gitlab_project_id = var.gitlab_project_id
  gitlab_username = var.gitlab_username
  gitlab_password = var.gitlab_token
  gitlab_graphql_api_url = var.gitlab_graphql_api_url
  agent_name = google_container_cluster.primary.name
  token_name = var.token_name
  token_description = var.token_description
}
